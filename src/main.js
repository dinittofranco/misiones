import 'bootstrap-css-only/css/bootstrap.min.css'
import 'mdbvue/lib/css/mdb.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'animate.css'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'
import firebase from 'firebase/app'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { VueSpinners } from '@saeris/vue-spinners'
import Vuelidate from 'vuelidate'
import '@/assets/app.css';

Vue.config.productionTip = false

Vue.use(VueSpinners)
Vue.use(Vuelidate)
Vue.use(BootstrapVue)

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyAssPwjXFMoJTaI0eOGnqZbXlyAx5k8ZmI",
    authDomain: "misiones-24.firebaseapp.com",
    projectId: "misiones-24",
    storageBucket: "misiones-24.appspot.com",
    messagingSenderId: "1030877066119",
    appId: "1:1030877066119:web:eaece0bed2bd8a12ec5922",
    measurementId: "G-CLXWFN0PSW"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);



new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')