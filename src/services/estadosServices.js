import axios from "axios";
const URL = 'http://177.87.111.134:3000/misiones/api/v1';


const getEstados = async() => {
    return await axios({
        method: "get",
        url: `${URL}/estados`,
    }).then((r) => r.data);
};

export default {
    getEstados,
}