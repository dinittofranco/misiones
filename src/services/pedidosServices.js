import axios from "axios";
const URL = 'http://177.87.111.134:3000/misiones/api/v1';

//Utimos cambios
const getPedidos = async() => {
    return await axios({
        method: "get",
        url: `${URL}/pedidos_pendientes`,
    }).then((r) => r.data);
};

const pedidoPorId = async(id) => {
    return await axios({
        method: "GET",
        url: `${URL}/pedido/${id}`,
    });
};


const updateEstadoPedido = async(estado, id) => {
    return await axios({
        method: "PUT",
        headers: {
            temp: 'aedb85b757c2cc2d9136335f45c7c5b4'
        },
        url: `${URL}/pedido/${id}`,
        data: { estado: estado }
    });
};

const updatePagoPedido = async(pago, id) => {
    return await axios({
        method: "PUT",
        headers: {
            temp: 'aedb85b757c2cc2d9136335f45c7c5b4'
        },
        url: `${URL}/pedidoPago/${id}`,
        data: { pago: pago },
    });
}

const deletePedido = async (id_pedido) => {
    return await axios({
        method: "DELETE",
        headers: {
            temp: 'aedb85b757c2cc2d9136335f45c7c5b4'
        },
        url: `${URL}/pedido/${id_pedido}`,
    });
}


export default {
    getPedidos,
    pedidoPorId,
    updateEstadoPedido,
    updatePagoPedido,
    deletePedido,
}