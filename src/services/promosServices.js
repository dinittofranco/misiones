import axios from "axios";
const URL = 'http://177.87.111.134:3000/misiones/api/v1';

const getPromos = async() => {

    return await axios({
        method: "get",
        url: `${URL}/promos`,
    }).then((r) => r.data);
};



const newPromo = async(aPromo) => {
    const data = new FormData();
    data.append("imgPromo", aPromo.imgPromo)
    data.append("nombre_promo", aPromo.nombre_promo)
    data.append("valor", aPromo.valor)
    data.append("productos", JSON.stringify(aPromo.productos))
    data.append("activa", aPromo.activa);
    data.append("tipo_producto", aPromo.tipo_producto);
    data.append("categoria", aPromo.categoria);

    return await axios({
        method: "POST",
        url: `${URL}/promo`,
        headers: {
            'Content-Type': 'multipart/form-data',
            'temp': 'aedb85b757c2cc2d9136335f45c7c5b4'
        },
        data: data,
    });
};

const editPromo = async(aPromo, id) => {
    const data = new FormData();
    //data.append("imgPromo", aPromo.imgPromo)
    data.append("nombre_promo", aPromo.nombre_promo)
    data.append("valor", aPromo.valor)
    data.append("productos", JSON.stringify(aPromo.productos))
    data.append("activa", aPromo.activa);
    data.append("tipo_producto", aPromo.tipo_producto);
    data.append("categoria", aPromo.categoria);

    return await axios({
        method: "PUT",
        url: `${URL}/promo/${id}`,
        headers: {
            'Content-Type': 'multipart/form-data',
            temp: 'aedb85b757c2cc2d9136335f45c7c5b4'

        },
        data: data,
    });
}

const deletePromo = async(id) => {
    return await axios({
        method: "POST",
        headers: {
            'temp': 'aedb85b757c2cc2d9136335f45c7c5b4'
        },
        url: `http://177.87.111.134:3000/misiones/api/v1/promo/${id}`,
    });
}

const promoPorId = async(id) => {
    return await axios({
        method: "GET",
        url: `${URL}/promo/${id}`,
    });
};

const getImagenPromo = async(id) => {
    (id);
    return await axios({
        method: "GET",
        url: `http://177.87.111.134:3000/misiones/api/v1/imagen/${id}`
    });
};


export default {
    getPromos,
    newPromo,
    promoPorId,
    getImagenPromo,
    editPromo,
    deletePromo,
}