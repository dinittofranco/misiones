import axios from "axios";
const URL = 'http://177.87.111.134:3000/misiones/api/v1';

const getCategorias = async() => {
    return await axios({
        method: 'GET',
        url: `${URL}/categorias`
    })
}


export default {
    //loginUsuario,
    getCategorias,
}