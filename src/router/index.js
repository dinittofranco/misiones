import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
import CrearPromo from '../views/CrearPromo.vue'
import EditarPromo from '../views/EditarPromo.vue'
import Promociones from '../views/Promociones.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [{
        path: '/login',
        name: 'Login',
        component: Login,
    },
    {
        path: '/',
        name: 'Home',
        component: Home,
        meta: { rutaProtegida: true }
    },
    {
        path: '/newpromo',
        name: 'CrearPromo',
        component: CrearPromo,
        meta: { rutaProtegida: true }
    },
    {
        path: '/editPromo/:id',
        name: 'EditarPromo',
        component: EditarPromo,
        meta: { rutaProtegida: true }
    },
    {
        path: '/promociones',
        name: 'Promociones',
        component: Promociones,
        meta: { rutaProtegida: true }
    },

    {
        path: '/editPedido/:id',
        name: 'EditarPedido',
        component: () => import( '../views/EditarPedido.vue' ),
        meta: { rutaProtegida: true }
    },

    {
        path: '/print/:id',
        name: 'PedidoPrint',
        component: () => import( '../views/PedidoPrint.vue' ),
        meta: { rutaProtegida: true }
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach( (to, from, next) => {
    const uid = JSON.parse( localStorage.getItem('usuario') );
    (to.meta.rutaProtegida)
    ? (
        (store.getters.autenticado || uid?.uid) 
            ? next()
            : next('/login')
        )
    : next();
    
});

export default router