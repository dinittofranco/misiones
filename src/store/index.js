import firebase from "firebase/app";
import "firebase/auth";

import Vue from 'vue';
import Vuex from 'vuex';
import categoriaStore from './categoriasApi'
import categoriasApi from './categoriasApi'


Vue.use(Vuex)

export default new Vuex.Store({
    //strict: process.env.NODE_ENV !== 'production',
    state: {
        usuario: {},
        autenticado: false,
    },

    actions: {
        async LOGIN_USUARIO({ commit }, usuario) {
            return await firebase
                .auth()
                .signInWithEmailAndPassword(usuario.email, usuario.password)
                .then(async (user) => {

                    const usuarioGuardar = {
                        email: usuario.email,
                        uid: user.user.uid
                    };
                    commit("SET_USUARIO", usuarioGuardar);
                    localStorage.setItem('usuario', JSON.stringify(usuarioGuardar));
                    commit("SET_AUTENTICADO", true);
                    return true;
                })
                .catch((error) => {
                    commit("SET_AUTENTICADO", false);
                    return false;
                });
        },

        async LOGOUT_USUARIO({ commit }) {
            await firebase
                .auth()
                .signOut()
                .then(function() {
                    commit("SET_USUARIO", { email: null, password: null }),
                    commit("SET_AUTENTICADO", false);
                    localStorage.clear();
                })
                .catch(function(error) {});
        },

    },

    mutations: {
        SET_USUARIO(state, user) {
            state.usuario = user;
        },

        SET_AUTENTICADO(state, aut) {
            state.autenticado = aut;
        },
    },

    getters: {
        usuario: (state) => state.usuario,
        autenticado: (state) => state.autenticado,
    },

    modules: {
        categoriasApi: categoriasApi,
    },
})