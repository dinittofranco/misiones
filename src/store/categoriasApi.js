"use strict";
import categoriasServices from '../services/categoriasServices'
export default {
    namespaced: true,
    state: {
        categorias: [],
    },

    actions: {
        async GET_CATEGORIAS({ commit }) {

            const res = await categoriasServices.getCategorias();
            (res);
            commit("SET_CATEGORIAS", res);
        },
    },

    mutations: {
        SET_CATEGORIAS(state, categorias) {
            state.categorias = categorias;
        },
    },


    getters: {
        categorias: (state) => state.categorias,
    },
};