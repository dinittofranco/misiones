import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyAssPwjXFMoJTaI0eOGnqZbXlyAx5k8ZmI",
    authDomain: "misiones-24.firebaseapp.com",
    projectId: "misiones-24",
    storageBucket: "misiones-24.appspot.com",
    messagingSenderId: "1030877066119",
    appId: "1:1030877066119:web:eaece0bed2bd8a12ec5922",
    measurementId: "G-CLXWFN0PSW"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();

const auth = firebase.auth()

export {auth}